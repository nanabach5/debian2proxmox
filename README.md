# Overview

ansble playbook to install proxmox virtual environment on Debian.  
(Now, only playbook to install PVE6 on Debian buster)

# description  
This playbook is based on this proxmox wiki page.
[Proxmon Wiki]https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_Buster

# How to use  

This playbook disables enterprose repository.
If you subscribe pve non-free plans, change state of 'disable enterprise repository' to 'present' before these process.

1. install debina to pve nodes
1. set static ip and edit /etc/hosts as this page. [Proxmox wiki]https://pve.proxmox.com/wiki/Install_Proxmox_VE_on_Debian_Buster#Add_an_.2Fetc.2Fhosts_entry_for_your_IP_address
1. add pve hosts to /etc/ansible/hosts on ansble server.
1. change 'hosts' parameter in buster2pve6.yml as you set in previous step.
1. execute playbook  
  - if you can ssh by root, "ansible-playbook debian2proxmox/buster2pve6.yml -u root -e 'ansible_python_interpreter=/usr/bin/python3'"  
  - if you can't ssh by root, "ansible-playbook debian2proxmox/buster2pve6.yml -u "some username you can ssh and su -" -k --ask-become-pass -e 'ansible_python_interpreter=/usr/bin/python3'"